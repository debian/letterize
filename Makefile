#
# Makefille for the phone-number letterizer
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

all: letterize

letterize: letterize.c

SOURCES = README COPYING NEWS letterize.adoc Makefile letterize.c

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

letterize-$(VERS).tar.gz: $(SOURCES) letterize.1
	@ls $(SOURCES) letterize.1 | sed s:^:letterize-$(VERS)/: >MANIFEST
	@(cd ..; ln -s letterize letterize-$(VERS))
	(cd ..; tar -czf letterize/letterize-$(VERS).tar.gz `cat letterize/MANIFEST`)
	@(cd ..; rm letterize-$(VERS))

clean:
	rm -f letterize letterize.tar letterize*.gz letterize*.rpm
	rm -f *~ *.1 *.html MANIFEST

install: uninstall letterize.1
	cp letterize /usr/bin
	cp letterize.1 /usr/share/man/man1/letterize.1

uninstall:
	rm -f /usr/bin/letterize /usr/share/man/man1/letterize.1

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

version:
	@echo $(VERS)

dist: letterize-$(VERS).tar.gz

release: letterize-$(VERS).tar.gz letterize.html
	shipper version=$(VERS) | sh -e -x

refresh: letterize.html
	shipper -N -w version=$(VERS) | sh -e -x
